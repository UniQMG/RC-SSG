# Really Cool Config-In-HTML Static Site Generator 2.0.0
Continuing my obsession of making the most fun SSG I can comes the RC(CIH)SSG: All configuration and output flow is done inside a special server-executed `<script>` tag in the input HTML files.

## Usage
```bash
yarn # (or npm install)
npm run build # node --experimental-vm-modules index.js
http-server # serves ./public
```

## Upgrading from 1.0.0
Add `await` to `files()` and `require()`

## How it works
Each HTML file in the input directory `in` is visited. Any
`<script type="ssg-control">` is executed in a soft sandbox
(es6 `import` available) where it can then load other files,
write multiple copies, or suppress the page entirely.

See comments in [index.html](in/index.html), [post.html](in/post.html) and
[template.html](in/template.html) for exact details.

## Other notes
Any non-HTML files are copied wholesale to the `out` directory, *unless*
they've been loaded using the `files(...)` function with `exclude=true`.

### Shared scripts
Shared scripts are similar to `ssg-control` scripts, but they're preserved
and run on both the client and server. Shared scripts can export a
`createApp(html, data)` function that returns a new Vue instance that will
be mounted on an element with the ID `#app`. This allows for client-side
hydration, and replaces the server-side only automatic Vue processing.

`provide`d or template-provided data is passed as the second argument,
and lives in a special `<script id="rc-ssg-data" type="application/json">`
in the rendered HTML. This is accessible even if you don't use `createApp`
for client-side Vue.

Shared scripts can `import` other es6 scripts, even on the server.

## Docs
- `async require(path: String, data: Object?)`:
  - When given a simple name: NodeJS require (npm package)
  - When given a relative path: Loads another SSG'd html file. `provide`d
    data and the contents of the `data` object will be provided to the
    loaded template. Returns an object with the following:
      - `$`: A cheerio selector for the semi-processed template file
        (after removing `ssg-control` scripts but before Vue)
      - All data `provide`d by the required file
      - `executions`: A list of final rendered forms of the output files
        (one per `output` call, or only one if `output` isn't called).
- `template(path: String)`: Specifies a template file, inserting existing
  `ssg-id="..."` nodes into `ssg-insert="..."`: nodes in the template.
  Any `provide`d or `output` variables will be accessible in the template.
  Calling this twice in the same document will only use the latter invocation
  (though templates can have their own templates).
- `provide(data: Object)`: Passes data to Vue for the standard copy.
- `output(path: String, data: Object)`: Writes a copy of the current file
  to the specified path and passes the given data to Vue. When data is a
  string, writes that string directly (bypassing Vue and `template`s).
- `vue(object: Object)`: Passes arbitrary arguments to the Vue constructor.
  Not compatible with a `createApp`-providing shared script.
- `async files(glob: String, options: Object)`: Loads files from disk.
  - `options.encoding` (default `utf8`): What encoding to pass to fs.readFile
  - `options.exclude` (default: `true`): Excludes the loaded files from the non-html
    bulk file copy process.
  - `options.read` (default: `true`): Reads the files' contents.
- `get(name: String)`: Gets a value provided by `provide`, `output`,
  `template`, or `require`.
- `silent()`: Suppresses output files. Implied on `template` and `require`d
  templates (though the template still needs its own `silent` if it isn't
  meant to be processed directly).
