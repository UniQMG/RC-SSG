import rrds from 'recursive-readdir-sync';
import vm from 'vm';
import fs from 'fs';
import path from 'path';
import glob from 'glob';
import VueSSR from 'vue-server-renderer';
import Vue from 'vue';
import mkdirp from 'mkdirp';
import rimraf from 'rimraf';
import deepmerge from 'deepmerge';
import { promisify as pr } from 'util';
import { JSDOM } from 'jsdom';

const outputDirectory = './public';
let excludedFiles = new Set();
rimraf.sync(outputDirectory);

class JSDOMWrapper {
  constructor(source) {
    let fulldoc = source.indexOf('<html') != -1;
    this.jsdom = fulldoc
      ? new JSDOM(source)
      : new JSDOM('<template id="ssg-fragment-template"></template>');
    if (!fulldoc)
      this.document.querySelector('template').innerHTML = source;
  }
  get document() {
    return this.jsdom.window.document;
  }
  get effectiveHead() {
    return (
      this.querySelector('head') ||
      this.document.querySelector('#ssg-fragment-template')
    );
  }
  querySelector(query) {
    let frag = this.document.querySelector('#ssg-fragment-template');
    if (frag) return frag.content.querySelector(query);
    return this.document.querySelector(query);
  }
  querySelectorAll(query) {
    let frag = this.document.querySelector('#ssg-fragment-template');
    if (frag) return frag.content.querySelectorAll(query);
    return this.document.querySelectorAll(query);
  }
  serialize() {
    let frag = this.document.querySelector('#ssg-fragment-template');
    if (frag) return frag.innerHTML;
    return this.jsdom.serialize();
  }
  toFullDocument() {
    let frag = this.querySelector('#ssg-fragment-template');
    if (frag) {
      frag.remove();
      for (let child of frag.children)
        this.document.body.appendChild(child);
    }
    return '<!DOCTYPE html>\n' + this.serialize();
  }
}

let start = Date.now();
console.log('Building files...');
await Promise.all(rrds('./in').map(async file => {
  if (!file.endsWith('.html')) return;
  await evaluateTemplate(file);
}));
console.log('Copying files...');
await Promise.all(rrds('./in').map(async file => {
  if (file.endsWith('.html')) return;
  if (excludedFiles.has(file)) return;
  let target = path.join(
    outputDirectory,
    file.replace(/\\/g, '/').split('/').slice(1).join('/')
  );
  await mkdirp(path.join(target, '..'));
  await pr(fs.copyFile)(file, target);
}));
console.log(`Done in ${Date.now() - start}ms!`);

async function evaluateTemplate(file, preprovidedData={}, asRequire=false) {
  const jsdom = new JSDOMWrapper(await pr(fs.readFile)(file, 'utf8'));

  let silent = asRequire;
  let executions = [];
  let vueExtras = {};
  let providedData = {...preprovidedData};
  let template = null;
  let ssrContext = vm.createContext({
    console: console,
    async require(mod, extData={}) {
      if (mod.startsWith('.')) {
        let data = {...providedData, ...extData};
        let val = await evaluateTemplate(path.join(file, '..', mod), data, true);
        return { ...val.providedData, $: val.$, executions: val.executions };
      }
      return await import(mod);
    },
    get $() {
      return $;
    },
    silent() {
      silent = true;
    },
    template(where) {
      template = path.join(file, '..', where);
    },
    async files(where, options={}) {
      let { read, encoding, exclude } = Object.assign({
        read: true,
        encoding: 'utf8',
        exclude: true
      }, options);

      let files = [];
      for (let subpath of await pr(glob)(path.join(file, '..', where))) {
        if (exclude) excludedFiles.add(subpath);
        files.push({
          path: path.relative(path.join(file, '..'), subpath).replace(/\\/g, '/'),
          name: subpath.split('/').slice(-1)[0],
          content: read ? await pr(fs.readFile)(subpath, encoding) : null
        });
      }
      return files;
    },
    output(outfile, data) {
      if (!outfile) throw new Error('Target file empty');
      executions.push({ outfile, data });
    },
    provide(dict) {
      Object.assign(providedData, dict);
    },
    get(val) {
      return providedData[val];
    },
    vue(extras) {
      vueExtras = deepmerge(vueExtras, extras);
    }
  });

  const modCaches = new Map();
  async function linker(context, specifier, referencingModule) {
    let target = './' + path.join(referencingModule.path, '..', specifier);

    if (!modCaches.has(referencingModule))
      modCaches.set(referencingModule, new Map());
    let modCache = modCaches.get(referencingModule);
    if (modCache.has(target))
      return modCache.get(target);

    let script = await pr(fs.readFile)(target, 'utf8');
    let module = new vm.SourceTextModule(script, { context });
    module.path = target;
    modCache.set(target, module);

    return module;
  }
  async function runModule(script, context) {
    const module = new vm.SourceTextModule(script, {
      identifier: 'inline script in ' + file,
      context
    });
    module.path = file;
    await module.link(linker.bind(null, context));
    await module.evaluate();
    return module.namespace;
  }

  // Evaluate server control scripts, then remove
  for (let script of jsdom.querySelectorAll('script[type="ssg-control"]')) {
    await runModule(script.innerHTML, ssrContext);
    script.remove();
  }

  if (executions.length == 0) { // Default execution
    executions.push({
      outfile: file.replace(/\\/g, '/').split('/').slice(1).join('/'),
      data: {}
    });
  }

  const preExecSource = jsdom.serialize();
  let rendered = await Promise.all(executions.map(async ({ outfile, data }) => {
    let jsdom = new JSDOMWrapper(preExecSource);

    if (typeof data == 'string') { // raw mode
      if (!silent) await writeFile(outfile, data);
      return data;
    }

    let executionData = {
      ...providedData,
      ...data,
      path: outfile.split('/').slice(1, -1).join(),
      pathToRoot: '../'.repeat(outfile.split('/').slice(1).length)
    };

    let execContext = vm.createContext({ console, Vue });
    let scripts = [];
    let createApp = null;
    // Evaluate shared scripts and move to head
    for (let script of jsdom.querySelectorAll('script[type="ssg-shared"]')) {
      let val = await runModule(script.innerHTML, execContext);
      if (val.createApp) {
        if (createApp) throw new Error('Found multiple createApp exports');
        createApp = val.createApp;
        script.id = 'ssg-app';
      }
      script.setAttribute('type', 'module');
      script.remove();
      scripts.push(script);
    }
    // Move any other scripts to head
    scripts.unshift(...[...jsdom.querySelectorAll('script')].map(script => {
      script.remove();
      return script;
    }));

    let finalUntemplatedHtml = createApp && jsdom.querySelector('#app').outerHTML;
    try {
      if (createApp) { // Custom shared app
        let app = createApp(finalUntemplatedHtml, executionData);
        const renderer = VueSSR.createRenderer();
        let rendered = await renderer.renderToString(app);
        jsdom.querySelector('#app').outerHTML = rendered;
      } else { // SSR only app
        let app = new Vue({
          template: jsdom.serialize(),
          data: executionData,
          ...vueExtras
        });
        const renderer = VueSSR.createRenderer();
        jsdom = new JSDOMWrapper(await renderer.renderToString(app));
      }
    } catch(err) {
      console.error('Error while evaluating template for ' + outfile);
      console.error(err);
      process.exit(1);
    }

    if (template) {
      let result = await evaluateTemplate(template, executionData, true);
      let { jsdom: subdoc, _html } = result.executions[0];
      for (let insertPoint of subdoc.querySelectorAll('[ssg-insert]')) {
        let optional = insertPoint.getAttribute('ssg-optional') != null;
        let targetId = insertPoint.getAttribute('ssg-insert');
        let content = jsdom.querySelector(`[ssg-id="${targetId}"]`);
        if (!content) {
          if (optional) {
            insertPoint.remove();
            continue;
          }
          throw new Error(`Failed to find templated element '${targetId}' in ${file}}`);
        }
        insertPoint.outerHTML = content.outerHTML;
      }
      subdoc.querySelector('#rc-ssg-data').remove();
      jsdom = subdoc;
    }

    function createScript(source, type, id, src) {
      let script = jsdom.document.createElement('script');
      if (source) script.innerHTML = source;
      if (type) script.setAttribute('type', type);
      script.setAttribute('defer', true);
      if (src) script.src = src;
      if (id) script.id = id;
      jsdom.effectiveHead.appendChild(script);
    }

    createScript(
      JSON.stringify({ html: finalUntemplatedHtml, data: executionData }),
      'application/json',
      'rc-ssg-data'
    );
    for (let script of scripts) {
      let source = script.innerHTML;
      if (script.id == 'ssg-app') {
        source += `
          let { html, data } = JSON.parse(document.getElementById('rc-ssg-data').innerText);
          let app = createApp(html, data);
          app.$mount('#app', true);
          window.app = app;
        `;
      }
      createScript(source, script.getAttribute('type'), script.id, script.src);
    }


    let html = jsdom.toFullDocument();
    if (!silent) await writeFile(outfile, html);
    return { jsdom, html };
  }));

  return { providedData, executions: rendered };
}

async function writeFile(outfile, contents) {
  await mkdirp(path.join(outputDirectory, outfile, '..'));
  await pr(fs.writeFile)(path.join(outputDirectory, outfile), contents);
}
