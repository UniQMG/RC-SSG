import SubComponent from './subcomponent.js';

export default {
  template: `<div>I'm a component! <sub-component/></div>`,
  components: { SubComponent }
}
